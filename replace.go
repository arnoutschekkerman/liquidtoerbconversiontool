package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type partial struct {
	name              string
	liquidPartialPath string
	content           []byte
}

//globals
var partialPath = "/Users/arnoutschekkerman/projects/sawadee/db/db_to_file/partials/"
var reader = bufio.NewReader(os.Stdin)
var newPartials []partial

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func formatFile(path string) {
	err := ioutil.WriteFile(path, formatContent(getPartialContent(path)), 0)
	check(err)
}

func formatContent(content []byte) []byte {

	// you can create new replacements like the one below:
	// newContents = strings.Replace(newContents, "old", "new", -1)

	//creates an space after opening
	newContents := string(content)
	newContents = strings.Replace(newContents, "{{", "<%= ", -1)
	newContents = strings.Replace(newContents, "{%", "<% ", -1)
	newContents = strings.Replace(newContents, "}}", " %>", -1)
	newContents = strings.Replace(newContents, "%}", " %>", -1)

	//remove double spaces(if the brackets already had a space the previous action would cause a double space)
	newContents = strings.Replace(newContents, "<%=  ", "<%= ", -1)
	newContents = strings.Replace(newContents, "<%  ", "<% ", -1)
	newContents = strings.Replace(newContents, "  %>", " %>", -1)
	newContents = strings.Replace(newContents, "  %>", " %>", -1)

	//partial
	newContents = strings.Replace(newContents, "<% include", "<%= render partial:", -1)
	//collection for partial rendering
	newContents = strings.Replace(newContents, "\" for ", "\", collection: ", -1)
	newContents = strings.Replace(newContents, "' for ", "', collection: ", -1)

	//cmsable, changes <% to <%=
	newContents = strings.Replace(newContents, "<% cms_able", "<%= cms_able", -1)

	//assign is not used in ruby and will be removed
	newContents = strings.Replace(newContents, " assign", "", -1)

	//contains
	newContents = strings.Replace(newContents, " contains ", ".include? ", -1)

	//ends
	newContents = strings.Replace(newContents, "endif", "end", -1)
	newContents = strings.Replace(newContents, "endfor", "end", -1)
	newContents = strings.Replace(newContents, "endunless", "end", -1)
	newContents = strings.Replace(newContents, "endif_present", "end", -1)
	newContents = strings.Replace(newContents, "end_present", "end", -1)
	newContents = strings.Replace(newContents, "endcase", "end", -1)

	//empty
	newContents = strings.Replace(newContents, " != empty", ".present?", -1)
	newContents = strings.Replace(newContents, " == empty", ".nil?", -1)

	//null
	newContents = strings.Replace(newContents, " != null", ".present?", -1)
	newContents = strings.Replace(newContents, " == null", ".nil?", -1)

	//filters
	newContents = strings.Replace(newContents, " | is_present", ".present?", -1)
	newContents = strings.Replace(newContents, " | first", ".first", -1)
	newContents = strings.Replace(newContents, " | minus:", " - ", -1)
	newContents = strings.Replace(newContents, " | plus: ", " + ", -1)
	newContents = strings.Replace(newContents, " | append:", " + ", -1)
	newContents = strings.Replace(newContents, " | divided_by:", " / ", -1)
	newContents = strings.Replace(newContents, " | modulo:", " % ", -1)
	newContents = strings.Replace(newContents, " | size", ".size ", -1)
	newContents = strings.Replace(newContents, " | downcase", ".downcase", -1)
	newContents = strings.Replace(newContents, " | upcase", ".upcase", -1)

	//var
	newContents = strings.Replace(newContents, " print_pdf ", " @print_pdf ", -1)
	newContents = strings.Replace(newContents, " print_if_pdf ", " @print_pdf ", -1)
	newContents = strings.Replace(newContents, " @print_if_pdf ", " @print_pdf ", -1)

	//generate trip url
	newContents = strings.Replace(newContents, "generate_trip_url:type", "generate_trip_url: type", -1)

	//variables
	newContents = strings.Replace(newContents, " show_belgian_version", "@show_belgian_version", -1)
	newContents = strings.Replace(newContents, "(trip)", "(@trip)", -1)
	newContents = strings.Replace(newContents, " trip ", " @trip ", -1)
	newContents = strings.Replace(newContents, " trip.", " @trip.", -1)
	newContents = strings.Replace(newContents, " print_params", " @print_params.present? and @print_params", -1)
	newContents = strings.Replace(newContents, " selected_action ", " @selected_action", -1)
	newContents = strings.Replace(newContents, " selected_action.", " @selected_action.", -1)

	// liquid for loops (optional limit)
	var re = regexp.MustCompile(`for\s([a-z_.@\d]*)\sin\s([a-z_.@\d\[\]]*)\s(limit:\s?([\d]*))?`)
	var matches = re.FindAllStringSubmatch(newContents, -1)
	//matches is a 2d array of match, match captures a for loop like this of which the whole capture is capturegroup1: (for capturegroup2 in capturegroup3)
	for _, match := range matches {
		fmt.Println("An for each loop has been created, you have to check if an each or a each with index is needed")
		if match[4] == "" {
			newContents = strings.Replace(newContents, match[0], match[2]+".each do |"+match[1]+"|", -1)
		} else {
			newContents = strings.Replace(newContents, match[0], match[2]+".first("+match[4]+").each do |"+match[1]+"|", -1)
		}
	}

	// if_present
	re = regexp.MustCompile(`if_present\s([@a-z_.\d]*)`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "if "+match[1]+".present?", -1)
	}

	// include with locals(if its a var with @ match[2] will create var: @var by doing match[3]: match[2]match[3] where match 2 is the '@')
	re = regexp.MustCompile(`<%=\srender\spartial:\s([a-z_"']*)\swith\s(@?)([a-z_.\d]*)`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "<%= render partial: "+match[1]+", locals: {"+match[3]+": "+match[2]+match[3]+"} %>", -1)
	}

	// cms_able
	re = regexp.MustCompile(`cms_able\s[^"|']([a-z_]*)`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "cms_able '"+match[1]+"'", -1)
	}

	// if @var to @var.present?
	re = regexp.MustCompile(`if\s(@[a-z_\d]*)\s%>`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "if "+match[1]+".present? %>", -1)
	}

	// to_euro_number(optional limited curency format)
	re = regexp.MustCompile(`([@a-z_.\d]*)\s\|\s(to_euro_number\s|to_euro_number: limited_currency)`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "to_euro_number("+match[1]+")", -1)
	}

	// slice
	re = regexp.MustCompile(`\s\|\sslice:\s([0-9], [0-9])`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "["+match[1]+"]", -1)
	}

	// textilize (replaces <%= with <%==)
	re = regexp.MustCompile(`<%=\s([@a-z_.\d]*)\s\|\stextilize`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "<%== textilize("+match[1]+")", -1)

	}

	// image_alt_text
	re = regexp.MustCompile(`([@a-z_.\d]*)\s\|\simage_alt_text`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "image_alt_text("+match[1]+")", -1)
	}

	//formatted date
	re = regexp.MustCompile(`([@a-z_.\d]*)\s\|\sformatted_date:\s(("|')[A-z _\-.%@\d]*("|'))`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "I18n.l("+match[1]+", format: "+match[2]+")", -1)
	}

	// date filter
	re = regexp.MustCompile(`([@a-z_.\d']*)\s\|\sdate:?\s?(["'A-z_\-%]*)?`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "I18n.l("+match[1]+", format: "+match[2]+")", -1)
	}

	// if print pdf
	re = regexp.MustCompile(`@print_pdf\s'([a-z="]*)' %>`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		newContents = strings.Replace(newContents, match[0], "if @print_pdf %> "+match[1]+" <% end %>", -1)
	}

	// valid html (replaces <%= with <%==)
	re = regexp.MustCompile(`<%=\s([@a-z_.\d]*)\s\|\svalid_html\s?(:\s?true)?`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		//1 or 2 arguments
		if match[2] == "" {
			newContents = strings.Replace(newContents, match[0], "<%== valid_html("+match[1]+")", -1)
		} else {
			newContents = strings.Replace(newContents, match[0], "<%== valid_html("+match[1]+", true)", -1)
		}
	}

	// generate_trip_url
	re = regexp.MustCompile(`([@a-z_.\d]*)\s\|\sgenerate_trip_url:?\s?([a-z_]*)?`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		// 1 or 2 arguments
		if match[2] == "" {
			newContents = strings.Replace(newContents, match[0], "generate_trip_url("+match[1]+")", -1)
		} else {
			newContents = strings.Replace(newContents, match[0], "generate_trip_url("+match[1]+",\""+match[2]+"\")", -1)
		}
	}

	// image_url
	re = regexp.MustCompile(`([@a-z_.\d]*)\s\|\simage_url:\s?("|')([a-z_]*),?([a-z_]*)("|')`)
	matches = re.FindAllStringSubmatch(newContents, -1)
	//var | image_url: "par1,par2" ---> cap0(cap1 | image_url cap2(")cap3,cap4") ---> var.generate_image_url("par1","par2")
	for _, match := range matches {
		//1 or 2 arguments for generate_image_url
		if match[3] == "" {
			newContents = strings.Replace(newContents, match[0], ""+match[1]+".generate_image_url(\""+match[3]+"\")", -1)
		} else {
			newContents = strings.Replace(newContents, match[0], ""+match[1]+".generate_image_url(\""+match[3]+"\",\""+match[4]+"\")", -1)
		}
	}

	return []byte(newContents)
}

func singleFileFormat() {
	for {
		fmt.Println("[Format only this file] Enter the path of the .erb file: ")
		path, _ := reader.ReadString('\n')
		if path == "\n" {
			fmt.Println("\n[Format only this file] PLEASE Enter a path please not nothing!")
		} else if filepath.Ext(path) != ".erb\n" && path != "\n" {
			fmt.Println(filepath.Ext(path))
			fmt.Println("[Format only this file] This file is not an .erb file")
		} else {
			path = strings.TrimSpace(path)
			fmt.Println("\n[Format only this file] Sure? press any key to start replacing (or 'ctrl c' to cancel)")
			_, _ = reader.ReadString('\n') //wait for input to start
			formatFile(path)
			break
		}
	}

	fmt.Println("[Format only this file] Done! don't forget to thoroughly check and reformat the code!")
}

func formatFolder() {
	for {
		fmt.Println("[Format this folder] Enter the folderpath")
		folderPath, _ := reader.ReadString('\n')
		if folderPath == "\n" {
			fmt.Println("\n[Format this folder] PLEASE Enter a path please not nothing!")
		} else {
			folderPath = strings.TrimSpace(folderPath)
			if folderPath[len(folderPath)-1:] != "/" {
				folderPath += "/"
			}
			fmt.Println("\n[Format this folder] Sure? press any key to start replacing (or 'ctrl c' to cancel)")
			_, _ = reader.ReadString('\n') //wait for input to start
			filePaths := getErbFilesInFolder(folderPath)
			for _, filepath := range filePaths {
				formatFile(filepath)
			}
			break
		}
	}
	fmt.Println("[Format this folder] Done! don't forget to thoroughly check and reformat the code!")
}

func getErbFilesInFolder(folderPath string) []string {
	var filePaths []string
	err := filepath.Walk(folderPath,
		func(path string, info os.FileInfo, err error) error {
			check(err)
			if filepath.Ext(path) == ".erb" {
				filePaths = append(filePaths, path)
			}
			return nil
		})
	check(err)
	return filePaths
}

func autoFormatEverything() {
	var startFilePath string
	var destinationPath string
	for {
		fmt.Println("\n[Auto format everthing] Enter the path of the .erb file to start with")
		startFilePath, _ = reader.ReadString('\n')
		startFilePath = strings.TrimSpace(startFilePath)
		if startFilePath == "\n" {
			fmt.Println("\n[Auto format everthing] enter a path please not nothing")
		} else if filepath.Ext(startFilePath) != ".erb" {
			fmt.Println("\n[Auto format everthing] This file is not an .erb file")
		} else {
			fmt.Println("\n[Auto format everthing] Enter the destination path for the new partials \n(press enter to use the same folder as the erb you just entered): ")
			destinationPath, _ = reader.ReadString('\n')
			if destinationPath == "\n" {
				destinationPath = strings.TrimSpace(destinationPath)

				if len(destinationPath) != 0 && destinationPath[len(destinationPath)-1] != '/' {
					destinationPath += "/"
				}

				destinationPath = filepath.Dir(startFilePath)

				fmt.Println("\n[Auto format everthing] Sure to reformat? press any key to start replacing (or 'ctrl c' to cancel)")
				_, _ = reader.ReadString('\n') //wait for input to start

				projectDir, err := getViewsFolderPath(destinationPath)
				check(err)

				for {
					formatFile(startFilePath)
					recursiveSearchAndFormat(projectDir, startFilePath, destinationPath)
					break
				}
				fmt.Println("[Auto format everthing] Done! don't forget to thoroughly check and reformat the code!")
				break
			}
		}
	}
}

func autoFormatEverythingInFolder() {
	for {
		fmt.Println("[Auto format everthing in folder] Enter the folderpath")
		folderPath, _ := reader.ReadString('\n')
		if folderPath == "\n" {
			fmt.Println("\n[Auto format everthing in folder] PLEASE Enter a path please not nothing!")
		} else {
			folderPath = strings.TrimSpace(folderPath)
			if folderPath[len(folderPath)-1:] != "/" {
				folderPath += "/"
			}
			fmt.Println("\n[Auto format everthing in folder] Sure? press any key to start replacing (or 'ctrl c' to cancel)")
			_, _ = reader.ReadString('\n') //wait for input to start

			projectDir, err := getViewsFolderPath(folderPath)
			check(err)

			filePaths := getErbFilesInFolder(folderPath)
			for _, filepath := range filePaths {
				formatFile(filepath)
				recursiveSearchAndFormat(projectDir, filepath, folderPath)
			}
			break
		}
	}
	fmt.Println("[Auto format everthing in folder] Done! don't forget to thoroughly check and reformat the code!")
}

func recursiveSearchAndFormat(projectDir, startFilePath, destinationPath string) {
	newPartials := scanFileForNewPartials(projectDir, startFilePath)
	if len(newPartials) != 0 {
		for _, newPartial := range newPartials {
			createNewPartial(destinationPath, newPartial)
			newPartialPath := destinationPath + "/_" + newPartial.name + ".html.erb"
			recursiveSearchAndFormat(projectDir, newPartialPath, destinationPath)
		}
	}
}

func getViewsFolderPath(destinationPath string) (string, error) {
	re := regexp.MustCompile(`([A-z\/]*views\/)`)
	matches := re.FindAllStringSubmatch(destinationPath, -1)
	if matches == nil {
		err := errors.New("Views folder not found in: " + destinationPath)
		return "", err
	}

	return matches[0][1], nil
}

func scanFileForNewPartials(projectDir string, startFilePath string) []partial {
	var partials []partial
	read, err := ioutil.ReadFile(startFilePath)
	check(err)
	newContents := string(read)

	//find all partials names in file
	var re = regexp.MustCompile(`render partial:\s["|']([a-z_0-9]*)["|']`)
	var matches = re.FindAllStringSubmatch(newContents, -1)
	for _, match := range matches {
		if !partialAlreadyExists(match[1], projectDir) {
			newPartial := partial{name: match[1], liquidPartialPath: getPathForPartialName(match[1])}
			newPartial.content = formatContent(getPartialContent(newPartial.liquidPartialPath))
			if newPartial.liquidPartialPath != "" {
				partials = append(partials, newPartial)
			}
		}
	}
	return removeDuplicatesInSlice(partials)
}

func getPathForPartialName(partialName string) string {
	//glob can return /partial_json_123 instead of partial_123 this will be regex'd out later
	paths, _ := filepath.Glob(partialPath + partialName + "*/html.html")

	if len(paths) != 0 {
		for _, path := range paths {
			if match, _ := regexp.MatchString(partialPath+partialName+"_[0-9]*\\/", path); match {
				return path
			}
		}
	}
	return ""
}

func getPartialContent(path string) []byte {
	read, err := ioutil.ReadFile(path)
	check(err)
	return read
}

func createNewPartial(destinationPath string, newPartial partial) {
	newPartialPath := destinationPath + "/_" + newPartial.name + ".html.erb"
	err := ioutil.WriteFile(newPartialPath, newPartial.content, 0755)
	check(err)
}

func partialAlreadyExists(partialName string, projectDir string) bool {
	var existingFileNames []string
	err := filepath.Walk(projectDir,
		func(path string, info os.FileInfo, err error) error {
			check(err)
			existingFileNames = append(existingFileNames, strings.TrimSuffix(strings.TrimPrefix(filepath.Base(path), "_"), ".html.erb"))
			return nil
		})
	check(err)
	for _, existingFileName := range existingFileNames {
		if partialName == existingFileName {
			return true
		}
	}
	return false
}

func removePartialFromSlice(s []partial, i int) []partial {
	s[i] = s[len(s)-1]
	// We do not need to put s[i] at the end, as it will be discarded anyway
	return s[:len(s)-1]
}

func removeDuplicatesInSlice(intSlice []partial) []partial {
	keys := make(map[string]bool)
	list := []partial{}
	for _, entry := range intSlice {
		if _, value := keys[entry.name]; !value {
			keys[entry.name] = true
			list = append(list, entry)
		}
	}
	return list
}

func main() {
	fmt.Println("\nLiquid to ERB Conversion Tool")
	fmt.Println("\nEnter the number to select what you want to convert:")
Loop:
	for {
		fmt.Println("[1] Single file\n[2] Whole folder\n[3] Single file and add the partials it uses\n[4] Whole folder and add the partials used by the partials in this folder")
		fmt.Println("")
		choice, _ := reader.ReadString('\n')
		choice = strings.TrimSpace(choice)
		switch choice {
		case "1":
			singleFileFormat()
			break Loop
		case "2":
			formatFolder()
			break Loop
		case "3":
			autoFormatEverything()
			break Loop
		case "4":
			autoFormatEverythingInFolder()
			break Loop
		default:
			fmt.Println("\nThat's not a correct number!")
			fmt.Println("")
		}
	}
}
